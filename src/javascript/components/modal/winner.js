import { showModal } from './modal'

export function showWinnerModal(fighter) {
  return showModal({
    title: 'Win__',
    bodyElement: fighter.name
  })
}
