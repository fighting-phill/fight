import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  function createProperty(keyValue) {
    const nameElement = createElement({ tagName: 'span', className: 'fighter-preview___property' });
    nameElement.innerText = keyValue.join(': ').replace(/(\w+):/, subStr => subStr.toUpperCase());
  
    return nameElement;
  }

  function createPreviewImage(source) {
    const attributes = { src: source };
    const imgElement = createElement({
      tagName: 'img',
      className: 'fighter-image___preview',
      attributes
    });

    if(position === 'right') {
      imgElement.style.transform = 'scale(-1, 1)';
    }
  
    return imgElement;
  }

  if(fighter) {
    const keyValueArray = Object.entries(fighter);
    fighterElement.append(createPreviewImage(fighter['source']));
    const fightPropsContainer = createElement({ tagName: 'div', className: 'fighter-preview___props-box' });
    keyValueArray
    .filter(keyValueAll => keyValueAll[0] !== '_id' && keyValueAll[0] !== 'source')
    .forEach(keyValue => fightPropsContainer.append(createProperty(keyValue)));
    fighterElement.append(fightPropsContainer);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

// const { name, health, attack, defense } = fighter;
// const fighterImage = createFighterImage(fighter);
// const fighterName = createElement({tagName: 'h1', className: 'fighter-preview___name'})
// const fighterHealth =  createElement({tagName: 'span', className: 'fighter-preview___param'});
// const fighterAttack =  createElement({tagName: 'span', className: 'fighter-preview___param'});
// const fighterDefense =  createElement({tagName: 'span', className: 'fighter-preview___param'});

// fighterName.textContent = name;
// fighterHealth.textContent = `Health: ${health}`
// fighterAttack.textContent = `Attack: ${attack}`
// fighterDefense.textContent = `Defense: ${defense}`
// fighterElement.append(fighterImage, fighterName, fighterHealth, fighterAttack, fighterDefense)
// return fighterElement;